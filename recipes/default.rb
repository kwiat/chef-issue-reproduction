#
# Cookbook:: reproduction
# Recipe:: default
#
# Copyright:: 2021, The Authors, All Rights Reserved.

bash 'echo' do
  compile_time true
  action :nothing
  code 'echo Writting from bash in compile_time after being notified'
end

bash 'send-notification' do
  compile_time true
  action :run
  notifies :run, 'bash[echo]', :immediately  
  code 'echo sending notification to bash[echo]'
end

